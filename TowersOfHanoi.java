//package towersofhanoi;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.ImageIcon;

public class TowersOfHanoi extends JFrame implements ActionListener, Runnable {

  ImageIcon img;

  public static void main(String[] args) {
    TowersOfHanoi toh = new TowersOfHanoi();
    toh.setVisible(true);
  }

  int n = 8;
  int fwidth = 1400, fheight = 600;
  JButton start = new JButton("Start");
  JButton exit = new JButton("EXit");
  Rectangle[] rod = new Rectangle[3];
  Rectangle[] disk = new Rectangle[n];
  JLabel numof_moves = new JLabel("Number of Moves: ");
  JLabel present_move = new JLabel("Present Move: ");
  JLabel expected_moves = new JLabel("Expected Moves: " + ((int) Math.pow(2, n) - 1));
  JTextArea log = new JTextArea();

  int[][] rod_capacity = new int[3][n];
  int[] h = new int[3];
  int num, count = 1;

  Thread t = new Thread(this);

  @Override
  public void run() {
    Hanoi(n, 1, 3, 2);
  }

  public TowersOfHanoi() {
    img = new ImageIcon("incode.png");
    setSize(img.getIconWidth(), img.getIconHeight());
    setLocationRelativeTo(null);
    setVisible(true);
    h[0] = n;
    h[1] = 0;
    h[2] = 0;
    setLayout(null);
    setSize(fwidth, fheight);
    setTitle("Towers Of Hanoi ");
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    JLabel logoIncode = new JLabel(null, img, JLabel.LEFT);
    start.setBounds(300, 520, 100, 25);
    exit.setBounds(600, 520, 100, 25);
    numof_moves.setBounds(100, 50, 300, 25);
    expected_moves.setBounds(300, 50, 300, 25);
    present_move.setBounds(500, 50, 300, 25);
    logoIncode.setBounds(800, 0, img.getIconWidth(), img.getIconHeight());
    log.setBounds(1000, 10, 400, 600);
    log.setEditable(false);
    add(start);
    add(exit);
    add(numof_moves);
    add(present_move);
    add(expected_moves);
    add(logoIncode);
    add(log);

    for (int i = 0; i < n; i++) {
      disk[i] = new Rectangle(150 + i * 12, 475 - i * 25, 200 - i * 25, 25);
      rod_capacity[0][i] = i; // pushing disk numbers in to first rod
    }

    rod[0] = new Rectangle(250, 200, 17, 300);
    rod[1] = new Rectangle(510, 200, 17, 300);
    rod[2] = new Rectangle(760, 200, 17, 300);

    start.addActionListener(this);
    exit.addActionListener(this);
  }

  public int dy(int x) {
    return fheight - x;
  }

  @Override
  public void actionPerformed(ActionEvent ae) {

    if (ae.getSource() == start) {
      t.start();

    }

    if (ae.getSource() == exit) {
      System.exit(0);
    }
  }

  public void Hanoi(int diskCount, int from, int dest, int aux) {
    if (diskCount == 1) {
      int hor_displacement = 260;
      try {
        Thread.sleep(1000);
        // maintains number of disks in each rod
        rod_capacity[dest - 1][h[dest - 1]] = rod_capacity[from - 1][--h[from - 1]];

        if ((from == 1 && dest == 3) || (from == 2 && dest == 3))
          hor_displacement = hor_displacement * 2;
        else if ((from == 3 && dest == 1) || (from == 2 && dest == 1))
          hor_displacement = 0;

        num = rod_capacity[dest - 1][h[dest - 1]++];
        disk[num].setLocation(150 + num * 12 + hor_displacement, 475 - (h[dest - 1] - 1) * 25);
        repaint();
        numof_moves.setText("Number of Moves :" + (count++));
        String moveString = "Disk " + (num + 1) + " moved from " + (char) (from + 64) + " --> " + (char) (dest + 64);
        present_move.setText("Present Move: " + moveString);
        setLog(moveString);
      } catch (Exception e) {
        System.out.println("Exception has been caught please investigate the issue");
        System.out.println(e);
      }
    } else {
      Hanoi(diskCount - 1, from, aux, dest);
      Hanoi(1, from, dest, aux);
      Hanoi(diskCount - 1, aux, dest, from);
    }
  }

  public void paint(Graphics g) {
    super.paint(g);
    g.setColor(Color.BLUE);

    for (int i = 0; i < 3; i++) {
      g.fillRect(rod[i].x, rod[i].y, rod[i].width, rod[i].height);
      g.drawString("" + (char) (i + 65), rod[i].x + 5, rod[i].y - 10);
    }

    g.drawLine(100, 500, 850, 500);

    // drawing disks
    for (int i = 0; i < n; i++) {
      g.setColor(Color.yellow);
      g.fillRoundRect(disk[i].x, disk[i].y, disk[i].width, disk[i].height, 10, 10);
      g.setColor(Color.BLACK);
      g.drawRoundRect(disk[i].x, disk[i].y, disk[i].width, disk[i].height, 10, 10);
      g.setColor(Color.black);
      g.drawString("" + (i + 1), disk[i].x + 100 - i * 12, disk[i].y + 16);
    }

  }

  public void setLog(String entry) {
    SimpleDateFormat s = new SimpleDateFormat("HH:mm:ss");
    Date d = new Date();

    log.append("[" + s.format(d) + "] " + entry + "\n");
    log.setCaretPosition(log.getDocument().getLength());
  }

}
